<cfscript>

	// Create fake table and row (id is just an integer field; no unique, non-null primary key for this)
	tblcontacts = queryNew("id, firstName, lastName, address, city, state, maxRate, maxAmount", "integer, varchar, varchar, varchar, varchar, varchar, decimal, double");
	queryAddRow(tblcontacts);
	querySetCell(tblcontacts, "id", 345);
	querySetCell(tblcontacts, "firstName", "Fakedata");
	querySetCell(tblcontacts, "lastName", "Fakedata");
	querySetCell(tblcontacts, "address", "Fakedata");
	querySetCell(tblcontacts, "city", "Fakedata");
	querySetCell(tblcontacts, "state", "XX");
	querySetCell(tblcontacts, "maxRate", 0.0);
	querySetCell(tblcontacts, "maxAmount", 0.00);


	// Check for valid JSON being posted
	if(!isDefined("FORM.json") || !isJson(FORM.json)) {
		writeOutput("No valid JSON posted.");
		abort;
	}

	// Prep the JSON for CF manipulation and test for valid data types/
	update = deserializeJSON(FORM.json);

	// Select proper row from fake table query (yes, I used select *, left out SQL injection checks, multiple row possibilities, etc)
	contact = new query(dbtype="query", sql="SELECT * FROM tblcontacts WHERE id = " & update.values.id);
	contact.setAttributes(tblcontacts=tblcontacts);
	res = contact.execute().getResult();

	// Update record with JSON values (since it's fake with a single test record, we're not actually saving anywhere or validating)
	res.firstName = update.values.firstName;
	res.lastName = update.values.lastName;
	res.address = update.values.address;
	res.city = update.values.city;
	res.state = update.values.state;
	res.maxRate = update.values.maxRate;
	res.maxAmount = update.values.maxAmount;

	// Pre-determined loans struct
	loans = [{
				"id" = 55677,
				"address" = "123 Fake St",
			 	"city" = "Saint Louis",
			 	"state" = "MO",
			 	"rate" =  8.0,
				"amount": 400000
			},
			{
				"id":9935,
				"address" =  "222 Main St",
				"city": "Boston",
				"state": "MA",
				"amount": 325500.00,
				"rate": 5.2
			},
			{
				"id":9936,
				"address": "111 Main St",
				"city": "Boston",
				"state": "MA",
				"amount": 500000.00,
				"rate": 3.9
			},
			{
				"id":9937,
				"address": "777 Main St",
				"city": "Chesterfield",
				"state": "MO",
				"amount": 125987.00,
				"rate": 4.0
			},
			{
				"id":9939,
				"address": "999 Main St",
				"city": "Albany",
				"state": "NY",
				"amount": 990987.00,
				"rate": 4.0
			}
	];

	// Create/populate results with proper matches to JSON data provided
	results = arrayNew(1);
	for(loan in loans) {
		if(loan.state == res.state && loan.rate < res.maxRate && loan.amount < res.maxAmount) {
			arrayAppend(results, loan);
		}
	}

	// Add result array to deserialized JSON and reserialize it all
	update.values.loans = results;
	tarball = serializeJSON(update);

	// Return JSON to caller
	writeOutput(tarball);

</cfscript>
